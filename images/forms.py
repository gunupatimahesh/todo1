
from django import forms


class ItemForm(forms.Form):
    image = forms.ImageField(
        label='Select a file'
    )