# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response,render,redirect
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from models import Item
from forms import ItemForm



def home(request):
    # Handle file upload
    if request.method == 'POST':
        form = ItemForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = Item(image=request.FILES['image'])
            newdoc.save()

            # Redirect to the document list after POST
            return render(request,'images/home.html')
    else:
        form = ItemForm()  # A empty, unbound form

    # Load documents for the list page
    documents = Item.objects.last()
    context_instance = RequestContext(request)

    # Render list page with the documents and the form
    return render(request,
        'images/home.html',
        {'documents': documents, 'form': form},context_instance

    )
def submit(request):
    document = Item(image=request.FILES['image'])
    document.save()
    return redirect('/')

    # template=loader.get_template('images/home.html')
    # contest={
    #     'image':Item.objects.get(id=3)
    # }
    # return HttpResponse(template.render(contest,request))




    # template=loader.get_template('home.html')
    # context={
    # context={
    #     'items':Item.objects.all()
    # }
    # return HttpResponse(template.render(context,request))

# def login(request,item_id):
#     template = loader.get_template('login.html')
#     context = {
#         'item': Item.objects.get(id=item_id )
#     }
#     return HttpResponse(template.render(context,request))