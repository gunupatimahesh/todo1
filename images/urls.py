from django.conf.urls import url
from . import views
from django.contrib.auth.views import login

urlpatterns =[
    url(r'^$',views.home,name='home'),
    url(r'^submit/$',views.submit,name='submit'),
]
# # -*- coding: utf-8 -*-
# from django.conf.urls import patterns , url
#
# urlpatterns = patterns(
#     'myproject.myapp.views',
#     url(r'^list/$', 'list', name='list'),
# )
